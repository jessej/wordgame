package com.laughingpants.wordstock;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jessejacobsen.wordgame.R;
import com.laughingpants.wordstock.pojo.Quiz;
import com.laughingpants.wordstock.pojo.Word;

public class QuizActivity extends Activity {
	private static final String KEY_CURRENT_WORD = "currentWordIndex";
	private static final String KEY_SHOWING_RESULTS =  "showingResults";
	
	private Quiz quiz;
	DatabaseHelper db;

	TextView txtHeader;
	RadioGroup rgSelectedAnswer;
	RadioButton[] rbAnswers;
	TextView txtQuizName;
	TextView txtQuizProgress;
	ProgressBar progressBar;
	Button btnPrevious;
	Button btnNext;
	ImageView imgCorrect;

	Word currentWord;
	RelativeLayout finishOverlay;
	RelativeLayout quizLayout;

	private boolean isReview;
	private boolean showingResults = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);
		
		findViews();
		
		// Get info from MainActivity
		Bundle extras = getIntent().getExtras();
		int quizId = extras.getInt(MainActivity.QUIZ_ID);
		isReview = extras.getBoolean(MainActivity.QUIZ_REVIEW);

		loadQuiz(quizId);
		
		// Jump to current progress within the quiz
		if (savedInstanceState == null) {
			currentWord = quiz.getWord(0);	
		} else {
			showingResults = savedInstanceState.getBoolean(KEY_SHOWING_RESULTS);
			if (showingResults) {
				showResults();
			} else {
				int currentWordIndex = savedInstanceState.getInt(KEY_CURRENT_WORD);
				quiz.setCurrentWordIndex(currentWordIndex);
				currentWord = quiz.getWord(currentWordIndex);
			}
		}
		
		udpateViews();
		setClickHandlers();
	}
	
	/**
	 * Load quiz from the db
	 */
	private void loadQuiz(int quizId) {
		db = new DatabaseHelper(this);
		quiz = db.getQuiz(quizId);
		quiz.shuffleDefinitions();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		// Save the current state of the quiz
		int currentWordIndex = quiz.getCurrentWordIndex();
		savedInstanceState.putInt(KEY_CURRENT_WORD, currentWordIndex);
		savedInstanceState.putBoolean(KEY_SHOWING_RESULTS, showingResults);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.onDestroy();
	}

	/**
	 * Find the views in the layout
	 */
	private void findViews() {
		txtHeader = (TextView) findViewById(R.id.txt_quiz_header);
		rgSelectedAnswer = (RadioGroup) findViewById(R.id.radio_group_selected_answer);
		txtQuizName = (TextView) findViewById(R.id.txt_quiz_name);
		txtQuizProgress = (TextView) findViewById(R.id.txt_quiz_progress);
		progressBar = (ProgressBar) findViewById(R.id.quiz_progress);
		btnPrevious = (Button) findViewById(R.id.button_previous);
		btnNext = (Button) findViewById(R.id.button_next);
		rbAnswers = new RadioButton[4];

		rbAnswers[1] = (RadioButton) findViewById(R.id.radio_1);
		rbAnswers[2] = (RadioButton) findViewById(R.id.radio_2);
		rbAnswers[3] = (RadioButton) findViewById(R.id.radio_3);

		finishOverlay = (RelativeLayout) findViewById(R.id.quiz_finish_overlay);
		imgCorrect = (ImageView) findViewById(R.id.img_correct);
		quizLayout = (RelativeLayout) findViewById(R.id.quiz_layout);
	}
	
	/**
	 * Set the click handlers for the views
	 */
	private void setClickHandlers() {
		btnPrevious.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				handlePreviousClick(v);
			}
		});

		btnNext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (quiz.getCurrentWordIndex() >= quiz.getWords().size() - 1) {
					handleFinishClick(v);
				} else {
					handleNextClick(v);
				}
			}
		});

		finishOverlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	/**
	 * Runs when user clicks previous button
	 */
	private void handlePreviousClick(View v) {
		setSelectedDefinitionId();

		currentWord = quiz.getPreviousWord();

		if (currentWord != null) {
			udpateViews();
		}
	}

	/**
	 * Runs when user clicks next button
	 */
	private void handleNextClick(View v) {
		// Set the quiz as in_progress
		if (quiz.getCurrentWordIndex() == 0
				&& quiz.getStatus() == Quiz.QuizStatus.NOT_STARTED) {
			quiz.setStatus(Quiz.QuizStatus.IN_PROGRESS);
			db.updateQuizStatus(quiz);
		}
		
		// Remember the users answer
		setSelectedDefinitionId();

		currentWord = quiz.getNextWord();

		udpateViews();
	}

	/**
	 * Finish the quiz or review
	 */
	private void handleFinishClick(View v) {
		if (!isReview) {
			// Save quiz and show results
			setSelectedDefinitionId();
			
			quiz.setStatus(Quiz.QuizStatus.COMPLETED);
			db.updateQuizStatus(quiz);

			showResults();
		} else {
			// Exit out if it's under review.
			finish();
		}
	}

	/**
	 * Show the results overlay
	 */
	private void showResults() {
		// Find views of the results overlay
		TextView txtResults = (TextView) finishOverlay
				.findViewById(R.id.quiz_finish_results);
		TextView txtPercent = (TextView) finishOverlay
				.findViewById(R.id.quiz_finish_percent);
		ProgressBar percentBar = (ProgressBar) finishOverlay
				.findViewById(R.id.quiz_finish_percent_progress);

		// Get score to display
		int score = quiz.getScore();
		int scorePercent = quiz.getScorePercent();

		// Display the quiz information
		finishOverlay.setVisibility(View.VISIBLE);
		txtResults.setText(getString(R.string.quiz_finish_results, score, quiz
				.getWords().size()));
		txtPercent
				.setText(getString(R.string.quiz_finish_percent, scorePercent));
		percentBar.setProgress(scorePercent);

		setQuip(scorePercent);

		// Used in for saving the state of the activity
		showingResults = true;
	}

	/**
	 * Sets the quip on the results overlay, based on final score
	 */
	private void setQuip(int scorePercent) {
		// index of result_quips_array is based on score
		int i = (int) Math.ceil((((double) scorePercent / 10)));
		String[] quips = getResources().getStringArray(
				R.array.result_quips_array);
		TextView txtQuip = (TextView) finishOverlay
				.findViewById(R.id.quiz_finish_quip);
		txtQuip.setText(quips[i]);
	}

	/**
	 * Sets the selected answer for the current question
	 */
	private void setSelectedDefinitionId() {
		int radioButtonID = rgSelectedAnswer.getCheckedRadioButtonId();
		View radioButton = rgSelectedAnswer.findViewById(radioButtonID);
		int selectedIndex = rgSelectedAnswer.indexOfChild(radioButton);
		int selectedId = currentWord.getDefinition(selectedIndex).getId();
		
		currentWord.setSelectedDefinitionId(selectedId);
		db.updateSelectedDefinitionId(currentWord);
	}

	/**
	 * Set selection of the current word
	 */
	private void updateRadioGroup() {
		int id = currentWord.getSelectedDefinitionId();

		// Radio indexing starts at 1
		for (int i = 0; i < currentWord.getDefinitions().size(); i++) {
			RadioButton r = rbAnswers[i + 1];
			r.setText(currentWord.getDefinition(i).getDefinition());
			if (isReview) {
				r.setEnabled(false);
			}
		}

		if (id == currentWord.getDefinitions().get(2).getId()) {
			rgSelectedAnswer.check(R.id.radio_3);
		} else if (id == currentWord.getDefinitions().get(1).getId()) {
			rgSelectedAnswer.check(R.id.radio_2);
		} else {
			rgSelectedAnswer.check(R.id.radio_1);
		}
	}

	/**
	 * Update all the views to represent current state
	 */
	private void udpateViews() {
		txtHeader.setText(currentWord.getWord());
		txtQuizName.setText(getString(R.string.quiz_name_text, quiz.getName()));
		txtQuizProgress.setText(getString(R.string.quiz_progress_text,
				quiz.getCurrentWordIndex() + 1, quiz.getWords().size()));
		updateButtons();
		updateRadioGroup();
		updateImgCorrect();
		progressBar.setProgress(quiz.getProgress());

	}

	/**
	 * Update the buttons based on the progress within the quiz
	 */
	private void updateButtons() {
		if (quiz.getCurrentWordIndex() <= 0) {
			btnPrevious.setEnabled(false);
		} else {
			btnPrevious.setEnabled(true);
		}

		if (quiz.getCurrentWordIndex() >= quiz.getWords().size() - 1) {
			btnNext.setText(R.string.button_finish);
		} else {
			btnNext.setText(R.string.button_next);
		}
	}

	/**
	 * Update the image to reflect whether the answer is correct or incorrect.
	 * Only when the quiz is being reviewed
	 */
	private void updateImgCorrect() {
		if (isReview) {
			if (currentWord.isCorrect()) {
				imgCorrect.setImageResource(R.drawable.check);
			} else {
				imgCorrect.setImageResource(R.drawable.x);
			}
		} else {
			imgCorrect.setVisibility(View.GONE);
		}
	}
}
