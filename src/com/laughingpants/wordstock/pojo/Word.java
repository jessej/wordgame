	package com.laughingpants.wordstock.pojo;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Jesse Jacobsen
 */
public class Word {
    private int id;
	private String word;
    private ArrayList<Definition> definitions;
    private int selectedDefinitionId;
    
    //
    // ---- Constructors
    //
    
    public Word() {
        definitions = new ArrayList<Definition>();
    }
    
    //
    // ---- Getters/Setters
    //
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
    public String getWord() {
        return word;
    }

	public void setWord(String word) {
        this.word = word;
    }
    
    public ArrayList<Definition> getDefinitions() {
        return definitions;
    }
    
 	public int getSelectedDefinitionId() {
		return selectedDefinitionId;
	}

	public void setSelectedDefinitionId(int selectedDefinitionId) {
		this.selectedDefinitionId = selectedDefinitionId;
	}   
    
	//
    // ---- Methods
    //

	/**
	 * Helper method to add a definition to a quiz
	 * @param definition {@link Definition} to add
	 */
	public void addDefinition(Definition definition) {
        definitions.add(definition);
    }
	
	/**
	 * Get a {@link Definition} by the index in the {@link ArrayList}
	 * @param index index of the definition
	 * @return {@link Definition}
	 */
	public Definition getDefinition(int index) {
		return definitions.get(index);
	}
    
	/**
	 * return true if the selected {@link Definition} is correct
	 * @return
	 */
    public boolean isCorrect() {
    	Definition selectedDefinition = getDefinitionById(selectedDefinitionId);
    	
		return selectedDefinition.isCorrect();
    }
    
    /**
     * Return a definition with a given id
     * @param searchId id to search for
     * @return {@link Definition} with that id, or null if none exists
     */
    public Definition getDefinitionById(int searchId) {
    	Definition returnVal = null;
    	for (Definition definition : definitions) {
    		if (definition.getId() == searchId) {
    			returnVal = definition;
    			break;
    		}
    	}
    	return returnVal;
    }
    
    /*
     * Randomizes the definitions
     */
    public void shuffleDefinitions() {
    	Collections.shuffle(definitions);
    }
}
