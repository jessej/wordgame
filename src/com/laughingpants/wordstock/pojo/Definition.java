package com.laughingpants.wordstock.pojo;
/**
 * 
 * @author JAJACOBSEN
 */
public class Definition {
	private int id;
	private String definition;
	private boolean isCorrect;

	public Definition() {
		
	}
	
	/**
	 * @return the isCorrect
	 */
	public boolean isCorrect() {
		return isCorrect;
	}

	/**
	 * @param isCorrect
	 *            the isCorrect to set
	 */
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	

}
