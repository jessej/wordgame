package com.laughingpants.wordstock.pojo;

import java.util.ArrayList;

public class Quiz {
	private ArrayList<Word> words;
	private int id;
	private String name;
	private int status;
	private int currentWordIndex;
	private int lastScore;
	
	//
	// ------ Constructor
	//
	
	public Quiz() {
		words = new ArrayList<Word>();
		currentWordIndex = 0;
	}

	
	//
	// ------ Getters/Setters
	//
	
	public int getCurrentWordIndex() {
		return currentWordIndex;
	}


	public void setCurrentWordIndex(int currentWordIndex) {
		this.currentWordIndex = currentWordIndex;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}
	
	public ArrayList<Word> getWords() {
		return words;
	}


	public void setWords(ArrayList<Word> words) {
		this.words = words;
	}
	
	public int getLastScore() {
		return lastScore;
	}


	public void setLastScore(int lastScore) {
		this.lastScore = lastScore;
	}
	
	//
	// ------ Methods
	//

	public Word getWord(int index) {
		return words.get(index);
	}
	
	/**
	 * Get the next word for the quiz. Increments currentWordIndex.
	 * @return The next word in the list, or null if there is none.
	 */
	public Word getNextWord() {
		currentWordIndex++;
		if (currentWordIndex >= words.size()) {
			return null;
		} else {
			return words.get(currentWordIndex);
		}
	}
	
	/**
	 * Get the previous word for the quiz. Decrements currentWordIndex.
	 * @return The previous word in the list, or null if there is none.
	 */
	public Word getPreviousWord() {
		currentWordIndex--;
		if (currentWordIndex < 0) {
			return null;
		} else {
			return words.get(currentWordIndex);
		}
	}
	
	/**
	 * Get the score of the quiz if it's finished, otherwise 0
	 * @return the number questions the player has gotten correct
	 */
	public int getScore() {
		int score = 0;
		
		if (status == QuizStatus.COMPLETED) {
			for (Word word : words) {
				if (word.isCorrect()) {
					score++;
				}
			}
		}
		
		return score;
	}
	
	/**
	 * Get the score of the quiz as a percent if it's finished, otherwise 0 
	 * @return the final score as a percentage
	 */
	public int getScorePercent() {
		float percent = ((float)getScore()) / (float)words.size();
		return (int) (percent * 100);
	}
	
	/**
	 * Get the progress within the quiz as an int 0 - 100.
	 * Based on currentWordIndex 
	 * @return The current progress
	 */
	public int getProgress() {
		float progress = (float)currentWordIndex / (float)words.size();
		return (int) (progress * 100);
	}
	
	/**
	 * Randomize definitions in each word
	 */
	public void shuffleDefinitions() {
		for (Word word : words) {
			word.shuffleDefinitions();
		}
	}
	
	//
	// ------ Enum type class
	//
	public class QuizStatus {
		/**
		 * Quiz hasn't been started
		 */
		public static final int NOT_STARTED = 0;
		/**
		 * At least on word has a selected definition
		 */
		public static final int IN_PROGRESS = 1;
		/**
		 * The quiz is completed
		 */
		public static final int COMPLETED = 2;
	}
};