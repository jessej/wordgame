package com.laughingpants.wordstock;

import java.util.HashMap;

import com.jessejacobsen.wordgame.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;

/**
 * Reusable prompt. Makes the code for creating modal dialog alert boxes a
 * little more readable.
 * 
 * @author jessej
 */
public class MyPrompt {
	private AlertDialog.Builder alert;

	private final Context context;
	private String defaultText = "";
	private HashMap<String, Object> infoMap;
	private EditText input;
	private boolean isConfirm = false;
	private String negativeText;
	private String positiveText;
	private boolean resetAfterShow = true;
	private String title;

	private OnClickListener positiveListener;
	private OnClickListener negativeListener;

	/**
	 * Basic constructor. Sets the title to an empty string
	 * 
	 * @param context
	 *            The calling activity
	 */
	public MyPrompt(Context context) {
		this(context, "");
	}

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            The calling activity.
	 * @param resourceId
	 *            Resource id of the String to use as the title.
	 */
	public MyPrompt(Context context, int resourceId) {
		this(context, context.getString(resourceId));
	}

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            The calling activity.
	 * @param title
	 *            The title of the dialog box.
	 */
	public MyPrompt(Context context, String title) {
		this.context = context;
		this.positiveText = context.getString(R.string.button_positive);
		this.negativeText = context.getString(R.string.button_negative);
		this.title = title;

		infoMap = new HashMap<String, Object>();

		createDialog();
	}

	/**
	 * Creates the dialog with the class variables. Uses AlertDialog.Builder.
	 */
	private void createDialog() {
		alert = new AlertDialog.Builder(context);

		if (!isConfirm) {
			input = new EditText(context);
			input.setText(defaultText);
			alert.setView(input);
			input.selectAll();
		}

		alert.setTitle(title);

		if (positiveListener == null) {
			if (context instanceof OnClickListener) {
				setPositiveListener((OnClickListener) context);
				setNegativeListener((OnClickListener) context);
			}
		}

		alert.setPositiveButton(positiveText, positiveListener);
		alert.setNegativeButton(negativeText, negativeListener);
	}

	public String getDefaultText() {
		return defaultText;
	}

	public Object getInfo(String key) {
		return infoMap.get(key);
	}

	/**
	 * @return The value of the input box.
	 * @throws RuntimeException
	 *             If the type of MyPrompt is a confirm.
	 */
	public String getInput() {
		if (isConfirm) {
			throw new RuntimeException("Can't get input of a confirm");
		} else {
			return input.getText().toString();
		}
	}

	/**
	 * Whether this prompt resets its parameters after it is shown.
	 * 
	 * @return boolean
	 */
	public boolean getResetAfterShow() {
		return resetAfterShow;
	}

	public boolean isConfirm() {
		return isConfirm;
	}

	/**
	 * Store arbitrary info that can be retrieved after the dialog box is
	 * closed.
	 * 
	 * @param key
	 *            A string for the key
	 * @param value
	 *            An generic object.
	 */
	public void putInfo(String key, Object value) {
		infoMap.put(key, value);
	}

	/**
	 * Reset the MyPrompt to its defaults.
	 */
	public void reset() {
		positiveText = context.getString(R.string.button_positive);
		negativeText = context.getString(R.string.button_negative);
		title = "";
		positiveListener = null;
		negativeListener = null;
		isConfirm = false;
		defaultText = "";
	}

	/**
	 * @param isConfirm
	 *            Whether this prompt shows a textbox for input
	 */
	public void setConfirm(boolean isConfirm) {
		this.isConfirm = isConfirm;
	}

	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}

	/**
	 * This will be called after the negative button is shown
	 * 
	 * @param negativeListener
	 *            A @DialogInterface.OnClickListener
	 */
	public void setNegativeListener(OnClickListener negativeListener) {
		this.negativeListener = negativeListener;
	}

	/**
	 * Text that shows up on the negative button. Defaults to
	 * R.string.button_negative
	 * 
	 * @param negativeText
	 *            A String
	 */
	public void setNegativeText(String negativeText) {
		this.negativeText = negativeText;
	}

	/**
	 * This will be called after the positive button is shown
	 * 
	 * @param positiveListener
	 *            A @DialogInterface.OnClickListener
	 */
	public void setPositiveListener(OnClickListener positiveListener) {
		this.positiveListener = positiveListener;
	}

	/**
	 * Text that shows up on the positive button. Defaults to
	 * R.string.button_positive
	 * 
	 * @param positiveText
	 *            A String
	 */
	public void setPositiveText(String positiveText) {
		this.positiveText = positiveText;
	}

	/**
	 * Whether this prompt resets its parameters after it is shown.
	 * 
	 * @return boolean
	 */
	public void setResetAfterShow(boolean resetAfterShow) {
		this.resetAfterShow = resetAfterShow;
	}

	/**
	 * The title of the prompt
	 * 
	 * @param resourceId
	 *            The String resource to show as title
	 */
	public void setTitle(int resourceId) {
		this.setTitle(context.getString(resourceId));
	}

	/**
	 * The title of the prompt
	 * 
	 * @param title
	 *            A String to use for the title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Creates and shows the dialog box. Optionally resets itself to default
	 * parameters after its shown. Note that value of the input and anything
	 * stored in info will remain.
	 */
	public void show() {
		createDialog();
		alert.show();
		if (resetAfterShow) {
			reset();
		}
	}
}
