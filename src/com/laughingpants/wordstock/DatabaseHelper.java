package com.laughingpants.wordstock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.jessejacobsen.wordgame.R;
import com.laughingpants.wordstock.pojo.Definition;
import com.laughingpants.wordstock.pojo.Quiz;
import com.laughingpants.wordstock.pojo.Word;

/**
 * Helper class for the WordGame database
 * 
 * @author Jesse Jacobsen
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String SQL_FILE = "WordGame.sql";
	private static final String DB_NAME = "WordGame.db";

	private static final String TABLE_WORD = "Word";
	private static final String KEY_WORD_ID = "word_id";
	private static final String KEY_WORD_WORD = "word";

	private static final String TABLE_QUIZ = "Quiz";
	private static final String KEY_QUIZ_ID = "quiz_id";
	private static final String KEY_QUIZ_NAME = "name";
	private static final String KEY_QUIZ_STATUS = "status";
	private static final String KEY_QUIZ_LAST_SCORE = "lastScore";

	private static final String TABLE_DEFINITION = "Definition";
	private static final String KEY_DEFINITION_ID ="definition_id";
	private static final String KEY_DEFINITION_DEFINITION = "definition";

	private static final String TABLE_QUIZ_WORD = "QuizWord";
	private static final String KEY_QUIZ_WORD_WORD_ID ="word_id";
	private static final String KEY_QUIZ_WORD_QUIZ_ID = "quiz_id";
	private static final String KEY_QUIZ_WORD_SELECTED_ANSWER ="selectedAnswer";

	private static final String TABLE_WORD_DEFINITION = "WordDefinition";
	//private static final String KEY_WORD_DEFINITION_WORD_DEFINITION_ID = "wordDefinition_id";
	private static final String KEY_WORD_DEFINITION_DEFINITION_ID = "definition_id";
	private static final String KEY_WORD_DEFINITION_WORD_ID = "word_id";
	private static final String KEY_WORD_DEFINITION_IS_CORRECT ="isCorrect";

	private Context context;

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		if (runSqlFile(context, db, SQL_FILE)) {
//			MainActivity.log("Database created successully");
		}
	}
	
	/**
	 * Get all the quizzes, This method returns only the quiz name and status
	 * for each quizz
	 * @return
	 */
	public ArrayList<Quiz> getQuizzes() {
		// Need
		// - number of questions
		// - percent complete
		// - final score (if applicable)
		
		// Get the names of each quiz
		
		SQLiteDatabase db = getReadableDatabase();
		String sql  = "Select * from " + TABLE_QUIZ;
		ArrayList<Quiz> quizzes = new ArrayList<Quiz>();
		Quiz quiz;
		
		Cursor c = db.rawQuery(sql, null);
		while (c.moveToNext()) {
			quiz = new Quiz();
			quiz.setId(c.getInt(c.getColumnIndex(KEY_QUIZ_ID)));
			quiz.setName(c.getString(c.getColumnIndex(KEY_QUIZ_NAME)));
			quiz.setStatus(c.getInt(c.getColumnIndex(KEY_QUIZ_STATUS)));
			quiz.setLastScore(c.getInt(c.getColumnIndex(KEY_QUIZ_LAST_SCORE)));
			
			quizzes.add(quiz);
		}
		
		return quizzes;
	}
	
	/**
	 * Load all the data for a quiz, including words and definitions
	 * @param id id of the quiz
	 * @return A Quiz object
	 */
	public Quiz getQuiz(int id) {
		SQLiteDatabase db = getReadableDatabase();
		Quiz quiz = null;
		String[] args = { String.valueOf(id) };
		Cursor c = db.query(TABLE_QUIZ, null, KEY_QUIZ_ID + "=?", args, null, null, null);
		
		if (c.getCount() >= 1) {
			c.moveToFirst();
			quiz = new Quiz();
			quiz.setId(c.getInt(c.getColumnIndex(KEY_QUIZ_ID)));
			quiz.setName(c.getString(c.getColumnIndex(KEY_QUIZ_NAME)));
			quiz.setStatus(c.getInt(c.getColumnIndex(KEY_QUIZ_STATUS)));
			quiz.setWords(getWords(quiz.getId()));
			quiz.setLastScore(c.getInt(c.getColumnIndex(KEY_QUIZ_LAST_SCORE)));
		}
		
		return quiz;
	}
	
	/**
	 * Get all matching words for a quiz
	 * @param quizId id of quiz to get words for
	 * @return All the words and corresponding definitions for a quiz
	 */
	public ArrayList<Word> getWords(int quizId) {
		SQLiteDatabase db = getReadableDatabase();		
		String sql = 
				"Select * from " + TABLE_QUIZ_WORD + " AS qw"
				+ " INNER JOIN " + TABLE_WORD + " AS w ON w." + KEY_WORD_ID + " = qw." + KEY_QUIZ_WORD_WORD_ID
				+ " INNER JOIN " + TABLE_WORD_DEFINITION + " AS wd ON wd." + KEY_WORD_DEFINITION_WORD_ID + " = w." + KEY_WORD_ID
				+ " INNER JOIN " + TABLE_DEFINITION + " AS d ON d." + KEY_DEFINITION_ID + " = wd." + KEY_WORD_DEFINITION_DEFINITION_ID
				+ " WHERE qw." + KEY_QUIZ_WORD_QUIZ_ID + " = ?"
				+ " ORDER BY w." + KEY_WORD_ID + " ASC";
		
		String[] args = new String[] { String.valueOf(quizId) };
		Cursor c = db.rawQuery(sql, args);
		ArrayList<Word> words = new ArrayList<Word>();
		Word word = new Word();
		Definition definition;
		int columnWordId = -1;
		
		while (c.moveToNext()) {
			columnWordId = c.getInt(c.getColumnIndex(KEY_QUIZ_WORD_WORD_ID));
			
			if (word.getId() != columnWordId) {
				word = new Word();
				word.setId(c.getInt(c.getColumnIndex(KEY_QUIZ_WORD_WORD_ID)));
				word.setWord(c.getString(c.getColumnIndex(KEY_WORD_WORD)));
				word.setSelectedDefinitionId(c.getInt(c.getColumnIndex(KEY_QUIZ_WORD_SELECTED_ANSWER)));
				words.add(word);
			}
			
			definition = new Definition();
			definition.setId(c.getInt(c.getColumnIndex(KEY_DEFINITION_ID)));
			definition.setDefinition(c.getString(c.getColumnIndexOrThrow(KEY_DEFINITION_DEFINITION)));
			definition.setCorrect(c.getInt(c.getColumnIndex(KEY_WORD_DEFINITION_IS_CORRECT)) == 1);
			
			word.addDefinition(definition);
		}
		
		return words;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Do nothing

	}

	/**
	 * Runs a sql file on a db object. Fails silently.
	 * 
	 * @param context
	 *            Reference to calling context.
	 * @param db
	 *            A SQLite database to work on
	 * @param sqlFile
	 *            A file containing the sql statements to execute. Note that
	 *            this function only allow ONE statement per line.
	 * @return returns true if the statements in the sql file were executed
	 *         successfully.
	 */
	private boolean runSqlFile(Context context, SQLiteDatabase db,
			String sqlFile) {
		InputStream is = null;
		BufferedReader br = null;
		String line = null;
//		int lineNumber = 1;

		try {
			is = context.getAssets().open(sqlFile);
			br = new BufferedReader(new InputStreamReader(is));

			while ((line = br.readLine()) != null) {
				db.execSQL(line);
//				lineNumber++;
			}

			return true;
		} catch (IOException ex) {
//			MainActivity.log("Error opening %s: %s", SQL_FILE, ex.toString());
			Toast.makeText(context, R.string.error_database, Toast.LENGTH_SHORT)
					.show();
			return false;
		} catch (SQLException ex) {
//			MainActivity.log(
//					"Error executing SQL Statement on line %d: %s - %s",
//					lineNumber, line, ex.toString());
			Toast.makeText(context, R.string.error_database, Toast.LENGTH_SHORT)
					.show();
			return false;
		} finally {
			try {
				is.close();
				br.close();
			} catch (Exception ex) {
//				MainActivity.log("Error closing stream readers: %s",
//						ex.toString());
				return false;
			}
		}
	}

	/**
	 * Update the selected definition id of a word in the db
	 * @param word The word to update
	 */
	public void updateSelectedDefinitionId(Word word) {
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(KEY_QUIZ_WORD_SELECTED_ANSWER, word.getSelectedDefinitionId());
		
		String[] args = new String[] { String.valueOf(word.getId()) };
		
		db.update(TABLE_QUIZ_WORD, cv, KEY_QUIZ_WORD_WORD_ID + "=?", args);
	}
	
	/**
	 * Update the status of the quiz in the db
	 * @param quiz The quiz to update
	 */
	public void updateQuizStatus(Quiz quiz) {
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(KEY_QUIZ_STATUS, quiz.getStatus());
		
		// Update score if the quiz is finished
		if (quiz.getStatus() == Quiz.QuizStatus.COMPLETED) {
			cv.put(KEY_QUIZ_LAST_SCORE, quiz.getScorePercent());
		}
		
		String[] args = new String[] {String.valueOf(quiz.getId()) };
		
		db.update(TABLE_QUIZ, cv, KEY_QUIZ_ID + "=?", args);
	}
	
	/**
	 * Close the database connection
	 */
	public void onDestroy() {
		close();
	}
}
