package com.laughingpants.wordstock;

import java.util.ArrayList;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.jessejacobsen.wordgame.R;
import com.laughingpants.wordstock.pojo.Quiz;

public class MainActivity extends Activity {
	public static final String QUIZ_ID = "quizid";
	public static final String QUIZ_REVIEW = "quizisreview";

	public static final String TAG = "WordGame";
	private DatabaseHelper db;
	private ArrayList<Quiz> quizzes;
	private ListView listview;

	MyPrompt myPrompt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		db = new DatabaseHelper(this);
		quizzes = db.getQuizzes();

		listview = (ListView) findViewById(R.id.listview);
		if (quizzes != null) {
			listview.setAdapter(new QuizAdapter(this, quizzes));
		}

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				handleClick(pos);
			}

		});
		
	}

	private void promptRetake(Quiz quiz) {
		myPrompt = new MyPrompt(this, R.string.quiz_retake_prompt_title);
		myPrompt.putInfo("quiz", quiz);
		myPrompt.setConfirm(true);
		myPrompt.setPositiveText(getString(R.string.quiz_retake));
		myPrompt.setPositiveListener(new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Quiz quiz = (Quiz) myPrompt.getInfo("quiz");
				startQuiz(quiz, false);
			}
		});

		myPrompt.setNegativeText(getString(R.string.quiz_review));
		myPrompt.setNegativeListener(new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Quiz quiz = (Quiz) myPrompt.getInfo("quiz");
				startQuiz(quiz, true);
			}
		});

		myPrompt.show();
	}

	private void handleClick(int pos) {
		Quiz quiz = quizzes.get(pos);

		if (quiz.getStatus() == Quiz.QuizStatus.COMPLETED) {
			promptRetake(quiz);
		} else {
			startQuiz(quiz, false);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		quizzes = db.getQuizzes();

		if (quizzes != null) {
			listview.setAdapter(new QuizAdapter(this, quizzes));
		}
	}

	private void startQuiz(Quiz quiz, boolean isReview) {
		Intent intent = new Intent(this, QuizActivity.class);
		intent.putExtra(QUIZ_ID, quiz.getId());
		intent.putExtra(QUIZ_REVIEW, isReview);

		startActivity(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.onDestroy();
	}

	/**
	 * Shortcut for logging. Uses TAG. Assumes String.Format parameters.
	 */
//	public static void log(String format, Object... args) {
//		Log.i(TAG, String.format(format, args));
//	}

	/**
	 * Shortcut for showing a {@link Toast}. Uses TAG. Assumes String.Format
	 * parameters.
	 */
	public void quickToast(String format, Object... args) {
		String text = String.format(format, args);
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}
}
