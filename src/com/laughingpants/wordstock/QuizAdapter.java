package com.laughingpants.wordstock;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jessejacobsen.wordgame.R;
import com.laughingpants.wordstock.pojo.Quiz;

/**
 * Adapter for quizzes
 * @author Jesse Jacobsen
 */
public class QuizAdapter extends android.widget.ArrayAdapter<Quiz> {
	private LayoutInflater inflater;
	private ArrayList<Quiz> quizzes;
	private Context context;

	public QuizAdapter(Context context, ArrayList<Quiz> quizzes) {
		super(context, android.R.layout.simple_list_item_2, quizzes);

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.context = context;
		this.quizzes = quizzes;
	}
	
	/**
	 * Gets the full string for the quiz's status
	 * @param quiz a {@link Quiz}
	 * @return The full status of the quiz
	 */
	private String getFullStatus(Quiz quiz) {
		String status = "";
		
		switch (quiz.getStatus()) {
		case Quiz.QuizStatus.NOT_STARTED:
			status = context.getString(R.string.quiz_status_not_started);
			break;
		case Quiz.QuizStatus.IN_PROGRESS:
			status = context.getString(R.string.quiz_status_in_progress);
			break;
		case Quiz.QuizStatus.COMPLETED:
			status = context.getString(R.string.quiz_status_finished);
			break;
		}
		
		return context.getString(R.string.quiz_status, status);
	}
	
	/**
	 * Get color based off quiz's status
	 * @param quiz a {@link Quiz}
	 * @return The color for the quiz status
	 */
	private int getStatusColor(Quiz quiz) {
		int color = Color.BLACK;
		Resources res = context.getResources();
		
		switch (quiz.getStatus()) {
		case Quiz.QuizStatus.NOT_STARTED:
			color = res.getColor(R.color.status_not_started);
			break;
		case Quiz.QuizStatus.IN_PROGRESS:
			color = res.getColor(R.color.status_in_progress);
			break;
		case Quiz.QuizStatus.COMPLETED:
			color = res.getColor(R.color.status_finished);
			break;
		}
		
		return color;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Quiz quiz = quizzes.get(position);
		
		if (convertView == null) {
			convertView = (View) inflater.inflate(
					R.layout.quiz_list_item, null);
		}
		
		TextView text1 = (TextView) convertView.findViewById(R.id.text1);
		TextView text2 = (TextView) convertView.findViewById(R.id.text2);
		TextView text3 = (TextView) convertView.findViewById(R.id.text3);
		
		text1.setText(quiz.getName());
		text2.setText(getFullStatus(quiz));
		text2.setTextColor(getStatusColor(quiz));
		
		if (quiz.getStatus() == Quiz.QuizStatus.COMPLETED) {
			text3.setText(context.getString(R.string.quiz_finish_percent,
					quiz.getLastScore()));
		} else {
			text3.setText("");
		}
		
		return convertView;
	}
}
